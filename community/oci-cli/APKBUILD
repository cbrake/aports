# Contributor: Adam Bruce <adam@adambruce.net>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=oci-cli
pkgver=3.37.12
pkgrel=0
pkgdesc="Oracle Cloud Infrastructure CLI"
url="https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm"
arch="noarch"
license="UPL-1.0 OR Apache-2.0"
depends="
	python3
	py3-arrow
	py3-certifi
	py3-click
	py3-cryptography
	py3-dateutil
	py3-jmespath
	py3-oci
	py3-openssl
	py3-prompt_toolkit
	py3-setuptools
	py3-six
	py3-terminaltables
	py3-tz
	py3-yaml
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/oracle/oci-cli/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # Cannot test as OCI resource identifiers are required as environment variables

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	find "$pkgdir"/usr/lib/python* -type d -name "tests" -exec rm -r {} \+
}

sha512sums="
d37a2d70b3fa539dff09d902521ac0c06d98c03adae43ef122c43eade81d698ad277172177066f3e25e91ba820fd12be4531136323c0b2dd5e45cc6d454407d0  oci-cli-3.37.12.tar.gz
"
