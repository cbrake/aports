# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-other
pkgname=haruna
pkgver=0.12.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/multimedia/haruna"
pkgdesc="Open-source video player built with Qt/QML and libmpv"
license="GPL-2.0-or-later AND GPL-3.0-or-later AND BSD-3-Clause"
depends="
	kirigami
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	mpv-dev
	ffmpeg-dev
	breeze-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kirigami-dev
	kxmlgui-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/haruna.git"
source="https://download.kde.org/stable/haruna/haruna-$pkgver.tar.xz
	0001-haruna-fix-KConfigGroup-QString-api.patch
	"

build() {
	cmake -B build -G Ninja \
		-DBUILD_WITH_QT6=ON \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3fe872654f29382c800670a7c8ec75e03de5c5dbe3c95dc6d77935a9eaa88aeeb41f61ac8593bafded8aaa9b6e63ced04a8bdaaa18efbca7375826abee4246d3  haruna-0.12.3.tar.xz
87dacd6b8fe59667ed69e6a4501ed629c32da494d5d439ee3c718971d127d4eca54a8b6d8bbdc55d25a8c56711b934b34ff352d1a9142b5aed9faaaa5bf92082  0001-haruna-fix-KConfigGroup-QString-api.patch
"
